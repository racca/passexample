# ImmuvidPass

A framework which handle the camera interaction and healthcard records for an 

## Installation

Use the swift package manager [pip](https://{username}@bitbucket.org/marinosoftware/immuvid-scanner-framework-ios.gi) to install foobar.

your main app also should include in your info.plist
```xml
    <key>NSCameraUsageDescription</key>
    <string>Required to scan QR codes</string>
```

## Usage

when you want to invoke an QRScan session you should call this piece of code

```swift
        ImmuvidPass.shared.start(present: self, animated: true) {
            result, error in
            ImmuvidPass.shared.dismiss(animated: true, completion: nil)
        }
```

where result 
is of the type QRCodeScanSession, you app should be able to read this

and if some Error occur an error is also included

## License
Marino Software Private use only
