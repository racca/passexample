// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.
//
import PackageDescription

let package = Package(
    name: "ImuvidPass",
    platforms: [.iOS(.v13)],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
             name: "ImmuvidPass",
             targets: ["ImuvidPassWrapper"]
           )
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(name: "JOSESwift", url: "https://github.com/airsidemobile/JOSESwift.git", from: .init("2.4.0")),
        .package(name: "FHIRModels", url: "git@github.com:apple/FHIRModels.git", from: .init("0.3.2")),
        .package(name: "Alamofire", url: "https://github.com/Alamofire/Alamofire", from: .init("5.4.4")),
        .package(name: "CertLogic", url: "https://github.com/eu-digital-green-certificates/dgc-certlogic-ios", branch: "main"),
        .package(name: "SwiftDGC", url: "https://github.com/eu-digital-green-certificates/dgca-app-core-ios.git", branch: "main"),
        .package(name: "FloatingPanel", url: "https://github.com/SCENEE/FloatingPanel", from: .init("2.4.1"))

    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "ImuvidPassWrapper",
            dependencies: [
                .target(name: "ImmuvidPass", condition: .when(platforms: [.iOS])),
                .product(name: "JOSESwift", package: "JOSESwift", condition: nil),
                .product(name: "ModelsR4", package: "FHIRModels", condition: nil),
                .product(name: "Alamofire", package: "Alamofire", condition: nil),
                .product(name: "CertLogic", package: "CertLogic", condition: nil),
                .product(name: "SwiftDGC", package: "SwiftDGC", condition: nil),
                .product(name: "FloatingPanel", package: "FloatingPanel", condition: nil)
                          ],
            resources: []
        ),
        .binaryTarget(
                   name: "ImmuvidPass",
                   path: "./Sources/ImuvidPassWrapper/ImmuvidPass.xcframework"
        )
    ]
)
