// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.5.1 (swiftlang-1300.0.31.4 clang-1300.0.29.6)
// swift-module-flags: -target arm64-apple-ios15.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -Onone -module-name ImmuvidPass
import AVFoundation
import Foundation
@_exported import ImmuvidPass
import JOSESwift
import ModelsR4
import PDFKit
import Swift
import SwiftDGC
import UIKit
import _Concurrency
public struct VerifiableCredential : Swift.Codable {
  public var credentialSubject: ImmuvidPass.CredentialSubject
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct CredentialSubject : Swift.Codable {
  public var fhirBundle: ModelsR4.Bundle
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct HealthCard : Swift.Codable {
  public var vc: ImmuvidPass.VerifiableCredential
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public struct Policy : Swift.Codable {
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
public enum PolicyType {
  case immunizations
  case testResults
  case none
  public static func == (a: ImmuvidPass.PolicyType, b: ImmuvidPass.PolicyType) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public enum PolicyResult {
  case green(policyType: ImmuvidPass.PolicyType)
  case orange(policyType: ImmuvidPass.PolicyType)
  case red(policyType: ImmuvidPass.PolicyType)
}
extension ModelsR4.Immunization {
  public var simpleCode: Swift.Int {
    get
  }
  public var mappedNameOrCode: Swift.String {
    get
  }
}
public struct TestResult {
}
public protocol QRScanDelegate {
  func scannerDecoded(_ string: Swift.String)
  func output(_ message: Swift.String)
}
@_hasMissingDesignatedInitializers public class ImmuvidPassManager {
  public static var shared: ImmuvidPass.ImmuvidPassManager
  public func startSession()
  public var scanSessionState: Swift.String {
    get
  }
  public typealias QRCodeScanSessionResult = (_ result: ImmuvidPass.ScanResult) -> Swift.Void
  public typealias QRCodeScanSessionOutput = (_ message: Swift.String) -> Swift.Void
  public func decodeQRCodeString(_ string: Swift.String, output: @escaping ImmuvidPass.ImmuvidPassManager.QRCodeScanSessionOutput, completion: @escaping ImmuvidPass.ImmuvidPassManager.QRCodeScanSessionResult)
  public func outputSessionState(output: (_ message: Swift.String) -> Swift.Void)
  public func fetchEcuadorPDF(from url: Foundation.URL, completion: @escaping (ImmuvidPass.EcuadorCertificateDetails?) -> Swift.Void)
  public func setPolicy(_ policy: ImmuvidPass.Policy)
  @objc deinit
}
public typealias Scans = [Swift.Int : Swift.String]
public struct QRCodeScanSession {
  public init()
  public enum ScanOutcome {
    case succesful
    case completed
    case alreadyScanned
    case chunkOutOfRange
    case invalidQRFormat
    case expectedChunksMismatch
    case containsURL(url: Foundation.URL)
    case containsEUPass(hc: SwiftDGC.HCert)
    case containsNumber(number: Swift.String)
  }
  public var statusDescription: Swift.String {
    get
  }
  public mutating func addScan(qrCode: Swift.String) -> ImmuvidPass.QRCodeScanSession.ScanOutcome
  public func buildHealthCard() throws -> ImmuvidPass.HealthCard
}
public enum QRError : Swift.Error {
  case signatureVerificationFailed
  case cantAssembleJWTifScanSessionIsIncomplete
  case QRCodeDoesNotContainASmartHealthCard
  public static func == (a: ImmuvidPass.QRError, b: ImmuvidPass.QRError) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public enum ScanResult {
  case success(pass: ImmuvidPass.Pass)
  case fail(error: ImmuvidPass.QRCodeError?)
}
public enum Pass {
  case smartHealthCardByImmunization(immunizations: [ModelsR4.Immunization])
  case smartHealthCardByObservations(observations: [ModelsR4.Observation])
  case dccPass(hc: SwiftDGC.HCert)
  case ecuadorPass(evc: ImmuvidPass.EcuadorCertificateDetails)
  case ivoryCoastPass(json: [[Swift.String : Any]])
}
public enum QRCodeError : Swift.Error {
  case unknown
  case unexpectedQRCode
  case invalidQRCode
  case policyNotSet
  public static func == (a: ImmuvidPass.QRCodeError, b: ImmuvidPass.QRCodeError) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public struct EcuadorCertificateDetails {
}
